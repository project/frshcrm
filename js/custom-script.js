console.log("Custom script library is running...");
var WebFormEvent =
{

  afterInstall: function()

  {
    console.log("After installation hook triggered.");
  }
  ,
  beforeSubmit: function()
  {

    console.log("Before submit hook triggered.");
  }
  ,
  afterSubmit: function(event, data, error)
  {
    if(error){ console.log("Error => ",error); }
    let email = JSON.parse(data).filter(e=> e.name == 'contact[email]');
    if(email.length != 0){
      fwcrm.trackCustomEvent("Drupal form submit",
      {
        "url": window.location.href
      })
    }

  }

}

jQuery("form").submit(function(event){
  console.log(event);
  let labels = [];
  jQuery(this).find('label').each(function(){
      labels.push(jQuery(this).text().toLowerCase().trim());
    });

  let ifEmail = labels.filter(e=> e == 'email');

  if(ifEmail.length != 0){

    fwcrm.trackCustomEvent("Drupal form submit",
    {
        "url": window.location.href
      })

  }
});
