<?php

namespace Drupal\freshworks_crm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contact Us admin configuraiton form.
 */
class FreshworksCRMAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'freshworks_crm.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'freshworks_crm_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('freshworks_crm.adminsettings');

    $form['freshworks_crm_admin_jssrc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Script URL'),
      '#description' => $this->t('To find the ScriptURL, Go to Admin Settings → CRM Tracking Code. Copy only the value from `src` parameter and paste it here. Sample URL : //in.fwcdn.com/xxxxxx/xxx.js'),
      '#default_value' => $config->get('freshworks_crm_admin_jssrc'),
      '#required' => TRUE,
    ];
    $form['freshworks_crm_admin_chat'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Chatbot'),
      '#description' => $this->t('To enable chatbot'),
      '#default_value' => $config->get('freshworks_crm_admin_chat'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $freshworks_crm_admin_jssrc = trim($form_state->getValue('freshworks_crm_admin_jssrc'));

    if ($this->startsWith($freshworks_crm_admin_jssrc, "//") && $this->endsWith($freshworks_crm_admin_jssrc, ".js")) {
      \Drupal::messenger()->addMessage($this->t('Successfully connected with freshworks CRM account'), 'status', TRUE);
    }
    else {
      if ($freshworks_crm_admin_jssrc) {
        $form_state->setErrorByName('freshworks_crm_admin_jssrc', $this->t('Incorrect Script URL format.'));
      }
      else {
        $form_state->setErrorByName('freshworks_crm_admin_jssrc', $this->t('Script URL is required.'));
      }
    }

  }

  // Function to check string starting.

  /**
   * With given substring.
   */
  public function startsWith($string, $startString) {
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
  }

  /**
   * Validate matched keyword.
   */
  public function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if (!$length) {
      return TRUE;
    }
    return substr($haystack, -$length) === $needle;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('freshworks_crm.adminsettings')
      ->set('freshworks_crm_admin_jssrc', trim($form_state->getValue('freshworks_crm_admin_jssrc')))
      ->set('freshworks_crm_admin_chat', trim($form_state->getValue('freshworks_crm_admin_chat')))
      ->save();
  }

}
