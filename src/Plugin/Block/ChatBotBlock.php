<?php

namespace Drupal\freshworks_crm\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "freshwork_chatbot",
 *   admin_label = @Translation("ChatBot Block"),
 * )
 */
class ChatBotBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $chatbot = \Drupal::config('freshworks_crm.adminsettings')->get('freshworks_crm_admin_chat');
    $jssrc = \Drupal::config('freshworks_crm.adminsettings')->get('freshworks_crm_admin_jssrc');

    if (isset($chatbot) && $chatbot == '1') {
      $renderHtml = "<script src='" . $jssrc . "' chat='true'></script>";
    }
    else {
      $renderHtml = "<script src='" . $jssrc . "' chat='false'></script>";
    }

    return [
      '#markup' => $renderHtml,
      '#allowed_tags' => ['script'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
